import requests


def use_requests(url, message, auth_token):
    response = requests.post(url, json=message, headers=auth_token)
    return response
